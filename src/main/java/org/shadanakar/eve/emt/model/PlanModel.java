/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import java.util.*;

public class PlanModel {
    private Map<Long, JobModel> jobsByItemId = new HashMap<>();
    private List<JobModel> jobs = new ArrayList<>();

//    public List<JobModel> getJobs() {
//        return Collections.unmodifiableList(jobs);
//    }

    public List<JobModel> addInventionJobs(List<EveItemNode> candidates, EveDecryptorInfo decryptorInfo, EveType baseItemType) {
        List<JobModel> addedJobs = new ArrayList<>(candidates.size());
        for (EveItemNode candidate : candidates) {
            for (EveItemNode.Entry entry : candidate.getItemInfoList()) {
                if (!jobsByItemId.containsKey(entry.getId())) {
                    JobModel jobModel = new JobModel(null, entry.getId(), candidate.getType(), ActivityType.INVENTION, 0, 0, 0, decryptorInfo, baseItemType); // not important
                    addedJobs.add(jobModel);
                    jobsByItemId.put(entry.getId(), jobModel);
                    jobs.add(jobModel);
                }
            }
        }

        return addedJobs;
    }

    public List<JobModel> addManufacturingJobs(List<EveItemNode> candidates, int materialLevel, int productivityLevel, int numberOfRuns) {
        List<JobModel> addedJobs = new ArrayList<>(candidates.size());
        for (EveItemNode candidate : candidates) {
            for (EveItemNode.Entry entry : candidate.getItemInfoList()) {
                if (!jobsByItemId.containsKey(entry.getId())) {
                    JobModel jobModel = new JobModel(null, entry.getId(), candidate.getType(), ActivityType.MANUFACTURING, materialLevel, productivityLevel, numberOfRuns, null, null);
                    addedJobs.add(jobModel);
                    jobsByItemId.put(entry.getId(), jobModel);
                    jobs.add(jobModel);
                }
            }
        }

        return addedJobs;
    }

    public void addJobs(List<JobModel> jobs) {
        for (JobModel job : jobs) {
            jobsByItemId.put(job.getItemId(), job);
            this.jobs.add(job);
        }
    }

    public Set<Long> getScheduledItemIds() {
        return Collections.unmodifiableSet(jobsByItemId.keySet());
    }

    public void removeJobs(List<JobModel> jobs) {
        this.jobs.removeAll(jobs);
        for (JobModel job : jobs) {
            jobsByItemId.remove(job.getItemId());
        }
    }

    public List<JobGroup> getJobGroups() {
        class JobGroupKey {
            private ActivityType activityType;
            private EveType blueprintType;
            private int materialLevel;
            private int productivityLevel;
            private int numberOfRuns;
            private EveDecryptorInfo decryptorInfo;
            private final EveType baseItemType;

            public JobGroupKey(ActivityType activityType, EveType blueprintType, int materialLevel, int productivityLevel, int numberOfRuns,
                               EveDecryptorInfo decryptorInfo, EveType baseItemType) {
                this.activityType = activityType;
                this.blueprintType = blueprintType;
                this.materialLevel = materialLevel;
                this.productivityLevel = productivityLevel;
                this.numberOfRuns = numberOfRuns;
                this.decryptorInfo = decryptorInfo;
                this.baseItemType = baseItemType;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof JobGroupKey)) return false;

                JobGroupKey that = (JobGroupKey) o;

                return materialLevel == that.materialLevel &&
                        productivityLevel == that.productivityLevel &&
                        numberOfRuns == that.numberOfRuns &&
                        activityType == that.activityType &&
                        blueprintType.getId().equals(that.blueprintType.getId()) &&
                        ((decryptorInfo == null && that.decryptorInfo==null) || (decryptorInfo != null && decryptorInfo.equals(that.decryptorInfo))) &&
                        ((baseItemType == null && that.baseItemType==null) || (baseItemType != null && baseItemType.equals(that.baseItemType)));

            }

            @Override
            public int hashCode() {
                int result = activityType.hashCode();
                result = 31 * result + blueprintType.getId().hashCode();
                result = 31 * result + numberOfRuns;
                result = 31 * result + materialLevel;
                result = 31 * result + productivityLevel;
                return result;
            }
        }

        Map<JobGroupKey, List<JobModel> > groups = new LinkedHashMap<>();

        for (JobModel job : jobs) {
            ActivityType activityType = job.getActivityType();
            EveType blueprintType = job.getBlueprintType();
            int materialLevel = job.getMaterialLevel();
            int productivityLevel = job.getProductivityLevel();
            int numberOfRuns = job.getNumberOfRuns();

            JobGroupKey key = new JobGroupKey(activityType, blueprintType, materialLevel, productivityLevel, numberOfRuns, job.getDecryptor(), job.getBaseItemType());

            if (!groups.containsKey(key)) {
                groups.put(key, new ArrayList<JobModel>());
            }

            groups.get(key).add(job);
        }

        // Convert
        List<JobGroup> res = new ArrayList<>(groups.size());
        for (Map.Entry<JobGroupKey, List<JobModel>> jobGroupKeyListEntry : groups.entrySet()) {
            JobGroupKey key = jobGroupKeyListEntry.getKey();
            List<JobModel> value = jobGroupKeyListEntry.getValue();
            res.add(new JobGroup(key.activityType, key.blueprintType, key.materialLevel, key.productivityLevel, key.numberOfRuns, key.decryptorInfo, key.baseItemType, value));
        }

        return res;
    }

    public boolean isItemScheduled(long itemId) {
        return jobsByItemId.containsKey(itemId);
    }
}
