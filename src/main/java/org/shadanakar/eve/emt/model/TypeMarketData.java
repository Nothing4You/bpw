/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import java.util.*;

public class TypeMarketData {
    private EveType type;
    private Date lastUpdated;
    private MarketData.Source source;
    private List<MarketDataOrderLine> buyOrders = new ArrayList<>();
    private List<MarketDataOrderLine> sellOrders = new ArrayList<>();
    private static final Comparator<MarketDataOrderLine> ascendingPriceComparator = new Comparator<MarketDataOrderLine>() {
        @Override
        public int compare(MarketDataOrderLine o1, MarketDataOrderLine o2) {
            return (int) Math.signum(o2.getPrice() - o1.getPrice());
        }
    };
    private static final Comparator<MarketDataOrderLine> descendingPriceComparator = new Comparator<MarketDataOrderLine>() {
        @Override
        public int compare(MarketDataOrderLine o1, MarketDataOrderLine o2) {
            return (int) Math.signum(o1.getPrice() - o2.getPrice());
        }
    };

    public TypeMarketData(EveType type, MarketData.Source source, Date lastUpdated) {
        this.type = type;
        this.source = source;
        this.lastUpdated = lastUpdated;
    }

    public void addLine(MarketOrderType orderType, float price, long quantity, long minVolume) {
        switch (orderType) {
            case Buy:
                buyOrders.add(new MarketDataOrderLine(price, quantity, minVolume));
                break;
            case Sell:
                sellOrders.add(new MarketDataOrderLine(price, quantity, minVolume));
                break;
            default:
                throw new RuntimeException("Oh No! This is not happening!");
        }
    }

    public void sort() {
        Collections.sort(buyOrders, ascendingPriceComparator);
        Collections.sort(sellOrders, descendingPriceComparator);
    }

    public List<MarketDataOrderLine> getOrders(MarketOrderType type) {
        return Collections.unmodifiableList(type == MarketOrderType.Buy ? buyOrders : sellOrders);
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public MarketData.Source getSource() {
        return source;
    }
}
