/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

public class JobModel {
    private Long id;
    private long itemId;
    private EveType blueprintType;
    private ActivityType activityType;
    private int materialLevel;
    private int productivityLevel;
    private int numberOfRuns;
    private EveDecryptorInfo decryptor;
    private final EveType baseItemType;

    public JobModel(Long id, long itemId, EveType blueprintType, ActivityType activityType, int materialLevel, int productivityLevel, int numberOfRuns,
                    EveDecryptorInfo decryptor, EveType baseItemType) {
        this.id = id;
        this.itemId = itemId;
        this.blueprintType = blueprintType;
        this.activityType = activityType;
        this.materialLevel = materialLevel;
        this.productivityLevel = productivityLevel;
        this.numberOfRuns = numberOfRuns;
        this.decryptor = decryptor;
        this.baseItemType = baseItemType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public long getItemId() {
        return itemId;
    }

    public EveType getBlueprintType() {
        return blueprintType;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public int getMaterialLevel() {
        return materialLevel;
    }

    public int getProductivityLevel() {
        return productivityLevel;
    }

    public int getNumberOfRuns() {
        return numberOfRuns;
    }

    public EveDecryptorInfo getDecryptor() {
        return decryptor;
    }

    @Override
    public String toString() {
        return "JobModel{" +
                "id=" + id +
                ", itemId=" + itemId +
                ", blueprintType=" + blueprintType +
                ", activityType=" + activityType +
                ", materialLevel=" + materialLevel +
                ", productivityLevel=" + productivityLevel +
                ", numberOfRuns=" + numberOfRuns +
                '}';
    }

    public static JobModel createInventionJob(Long id, long itemId, EveType blueprintType, EveDecryptorInfo decryptor, EveType baseItemType) {
        return new JobModel(id, itemId, blueprintType, ActivityType.INVENTION, 0, 0, 0, decryptor, baseItemType);
    }

    public EveType getBaseItemType() {
        return baseItemType;
    }
}
