/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.model;

import java.util.*;

public class JobWorkflowItem {
    private final ActivityType activityType;
    private final EveType blueprintType;
    private float quantity;
    private Map<Integer, EveMaterialType> materialTypes;
    private List<JobWorkflowItem> dependedWorkflowItems = new ArrayList<>();
    private int productivityLevel;

    public JobWorkflowItem(ActivityType activityType, EveType blueprintType, int productivityLevel, float quantity, List<EveMaterialType> materialTypes) {
        this.activityType = activityType;
        this.blueprintType = blueprintType;
        this.productivityLevel = productivityLevel;
        this.quantity = quantity;
        this.materialTypes = new LinkedHashMap<>();
        if (materialTypes != null) {
            for (EveMaterialType materialType : materialTypes) {
                this.materialTypes.put(materialType.getMaterialType().getId(), materialType);
            }
        }
    }

    public void addDependingItem(JobWorkflowItem dependingItem) {
        dependedWorkflowItems.add(dependingItem);
    }

    public EveType getProductType() {
        return blueprintType.getBlueprint().getProductType();
    }

    public float getQuantity() {
        return quantity;
    }

    public EveMaterialType getMaterialTypeById(Integer typeId) {
        return materialTypes.get(typeId);
    }

    public List<JobWorkflowItem> getDependedWorkflowItems() {
        return Collections.unmodifiableList(dependedWorkflowItems);
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public EveType getBlueprintType() {
        return blueprintType;
    }

    public int getProductivityLevel() {
        return productivityLevel;
    }

    public ActivityType getActivityType() {
        return activityType;
    }
}
