/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EveXmlParser<T> {
    private static final Log log = LogFactory.getLog(EveXmlParser.class);

    public static interface Callback<E> {
        public static abstract class PlainListAdapter<F> implements Callback<F> {
            @Override
            public void started() {}

            @Override
            public final boolean isExpandRowSupportedFor(F value) {
                log.warn("It appears to be non-plan xml after all, please consider implementing Callback interface instead.");
                return false;
            }

            @Override
            public final F addChildren(F value, List<F> children) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void finished(List<F> rootRowsetItems) {}
        }
        void started();
        void setDocumentTimestamps(Date currentTime, Date cachedUntil);
        E createRowObject(Map<String, String> attributes, boolean hasChildren) throws SQLException;
        boolean isExpandRowSupportedFor(E value);
        E addChildren(E value, List<E> children);
        void finished(List<E> rootRowsetItems);
    }

    private final String rootNodeName;

    public EveXmlParser() {
        this("eveapi");
    }

    public EveXmlParser(String rootNodeName) {
        this.rootNodeName = rootNodeName;
    }

    public void parse(Reader reader, Callback<T> callback) throws ParsingException, IOException, SQLException {
        Builder builder = new Builder();
        Document doc = builder.build(reader);

        callback.started();

        String root = "/" + rootNodeName;
        Nodes currentTimeNodes = doc.query(root + "/currentTime");
        Nodes cachedUntilNodes = doc.query(root +"/cachedUntil");
        Element currentTimeElement = (Element) currentTimeNodes.get(0);
        Element cachedUntilElement = cachedUntilNodes.size() != 0 ? (Element) cachedUntilNodes.get(0) : currentTimeElement;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.ENGLISH);
        Date currentTime = new Date(0);
        Date cachedUntil = new Date(0);
        try {
            currentTime = dateFormat.parse(currentTimeElement.getValue() + " UTC");
            cachedUntil = dateFormat.parse(cachedUntilElement.getValue() + " UTC");
        } catch (ParseException e) {
            log.error("Date strings have invalid format: [" + currentTimeElement.getValue() + ", " + cachedUntilElement.getValue()
                    + "], expected format is yyyy-MM-dd HH:mm:ss", e);
        }

        callback.setDocumentTimestamps(currentTime, cachedUntil);

        Nodes res = doc.query(root + "/result/rowset");
        Element rootRowset = (Element) res.get(0);

        List<T> rootItems = processRowset(rootRowset, callback);

        callback.finished(rootItems);
    }

    private List<T> processRowset(Element rowsetElement, Callback<T> callback) throws SQLException {
        Elements childElements = rowsetElement.getChildElements("row");

        List<T> values = new ArrayList<>();
        for (int i = 0, childElementCount=childElements.size(); i < childElementCount; ++i) {
            Element row = childElements.get(i);
            Map<String, String> attributes = new HashMap<>();

            for(int j=0, attributeCount = row.getAttributeCount(); j < attributeCount; ++j) {
                Attribute attribute = row.getAttribute(j);
                String attributeName = attribute.getLocalName();
                String attributeValue = attribute.getValue();
                attributes.put(attributeName, attributeValue);
            }

            Element nestedRowsetElement = row.getFirstChildElement("rowset");

            T value = callback.createRowObject(attributes, nestedRowsetElement != null);

            if(nestedRowsetElement != null) {
                // it has rowset inside... lets ask callback if it needs it
                if(callback.isExpandRowSupportedFor(value)) {
                    List<T> children = processRowset(nestedRowsetElement, callback);
                    value = callback.addChildren(value, children);
                }
            }

            values.add(value);
        }

        return values;
    }

}
