/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.shadanakar.eve.emt.model.*;

/**
 * Calculates stuff...
 */
public class BlueprintHelper {
    public static LevelInfo calculateMaterialLevel(LocalBlueprintInfo localBlueprintInfo, EveBlueprint blueprint, BlueprintType blueprintType) {
        if (localBlueprintInfo.getMaterialLevel() != null) {
            return new LevelInfo(localBlueprintInfo.getMaterialLevel(), LevelSource.TYPE);
        } else {
            return new LevelInfo(blueprintType == BlueprintType.Original || blueprint.getTechLevel() != 2 ? 0 : -4, LevelSource.DEFAULT);
        }
    }

    public static int calculateRuns(LocalBlueprintInfo localBlueprintInfo, EveBlueprint blueprint) {
        return localBlueprintInfo.getRuns() == null ?
                (blueprint.getTechLevel() != 2 ? blueprint.getMaxProductionLimit() : blueprint.getMaxProductionLimit()/10) :
                localBlueprintInfo.getRuns();
    }

    public static LevelInfo calculateProductivityLevel(LocalBlueprintInfo localBlueprintInfo, EveBlueprint blueprint, BlueprintType blueprintType) {
        if (localBlueprintInfo.getProductivityLevel() != null) {
            return new LevelInfo(localBlueprintInfo.getProductivityLevel(), LevelSource.TYPE);
        } else {
            return new LevelInfo(blueprintType == BlueprintType.Original || blueprint.getTechLevel() != 2 ? 0 : -4, LevelSource.DEFAULT);
        }
    }

    public static float calculateProductionTime(int productivityLevel, EveBlueprint blueprint) {
        final float PTM = 0.8f; // industry 5, no implants, time multiplier=1.0

        if (productivityLevel >= 0) {
            return blueprint.getProductionTime()*(1.0f - (blueprint.getProductivityModifier()/(float)blueprint.getProductionTime())*(productivityLevel/(1.0f+productivityLevel)))*PTM;
        } else {
            return blueprint.getProductionTime()*(1.0f - (blueprint.getProductivityModifier()/(float)blueprint.getProductionTime())*(productivityLevel-1.0f))*PTM;
        }
    }

    public static float calculateWasteFactor(EveBlueprint blueprint, int materialLevel) {
        float baseWasteFactor = blueprint.getWasteFactor()/100.0f;
        float wasteFactor;
        if (materialLevel < 0) {
            wasteFactor = (1.0f-materialLevel)*baseWasteFactor;
        } else {
            wasteFactor = baseWasteFactor*(1.0f/(materialLevel+1));
        }

        return wasteFactor;
    }

    public static float calculateInventionTime(EveType blueprintType) {
        return blueprintType.getBlueprint().getResearchTechTime();
    }

    public static LocalBlueprintInfo calculateT2LocalBlueprintInfo(EveType outcomeBlueprintType, EveDecryptorInfo decryptor) {
        LocalBlueprintInfo result = new LocalBlueprintInfo(outcomeBlueprintType.getId());
        result.setMaterialLevel(-4 + (decryptor == null ? 0 : (int) decryptor.getMeModifier()));
        result.setProductivityLevel(-4 + (decryptor == null ? 0 : (int)  decryptor.getPeModifier()));
        result.setRuns((outcomeBlueprintType.getBlueprint().getMaxProductionLimit()/10) +
                (decryptor == null ? 0 : (int) decryptor.getMaxRunModifier()));

        // there
        return result;
    }

    public static float calculateInventionProbability(EveType blueprintType, EveDecryptorInfo decryptor, EveType baseItemType) {
        float encryptionSkillLevel = 5;
        int datacore1SkillLevel = 5;
        int datacore2SkillLevel = 5;
        int metaLevel = baseItemType == null ? 0 : baseItemType.getMetaLevel();

        return Math.min(1.0f, blueprintType.getBlueprint().getChanceOfInvention() * (decryptor == null ? 1.0f : decryptor.getProbabilityMultiplier())
                * (1 + 0.01f * encryptionSkillLevel)
                * (1 + (datacore1SkillLevel + datacore2SkillLevel) * 0.1f / (5 - metaLevel)));
    }

    public static float calculateCopyTime(EveType blueprintType) {
        return blueprintType.getBlueprint().getResearchCopyTime();
    }
}
