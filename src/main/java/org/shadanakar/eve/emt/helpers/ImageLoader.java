/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.serialization.SerializationException;
import org.apache.pivot.wtk.media.BufferedImageSerializer;
import org.apache.pivot.wtk.media.Picture;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ImageLoader {
    private static final Log log = LogFactory.getLog(ImageLoader.class);

    private final Map<Integer, Picture> iconCache = Collections.synchronizedMap(new HashMap<Integer, Picture>());

    private ImageLoader() {}

    public Picture loadImage16x16(Integer typeId) {
        if (!iconCache.containsKey(typeId)) {
            Path iconLocation = Constants.eveItemsPath.resolve(typeId + "_32.png");
            try {
                Picture icon = loadImageFromFile(iconLocation);
                icon.resample(16);
                iconCache.put(typeId, icon);
            } catch (SerializationException | IOException e) {
                log.warn("Unable to load image: " + iconLocation, e);
                return null;
            }
        }

        return iconCache.get(typeId);
    }

    private Picture loadImageFromFile(Path path) throws IOException, SerializationException {
        try(InputStream inputStream = new BufferedInputStream(new FileInputStream(path.toFile()))) {
            BufferedImageSerializer serializer = new BufferedImageSerializer();
            BufferedImage bufferedImage = serializer.readObject(inputStream);
            return new Picture(bufferedImage);
        }
    }

    public static ImageLoader getInstance() {
        return Holder._instance;
    }

    public Path getImage64x64Path(Integer typeId) {
        return Constants.eveItemsPath.resolve(typeId + "_64.png");
    }

    public Path getImage32x32Path(Integer typeId) {
        return Constants.eveItemsPath.resolve(typeId + "_32.png");
    }

    private static class Holder {
        private final static ImageLoader _instance = new ImageLoader();
    }
}
