/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import nu.xom.ParsingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.util.ListenerList;
import org.apache.pivot.wtk.WTKListenerList;
import org.shadanakar.eve.emt.model.EveAssets;
import org.shadanakar.eve.emt.model.EveIndustryJob;
import org.shadanakar.eve.emt.model.IndustryJobModel;
import org.shadanakar.eve.emt.ui.UserSettings;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.utils.SyncUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

public class AssetManager {
    private static final Log log = LogFactory.getLog(AssetManager.class);

    private static final ReentrantLock lock = new ReentrantLock();

    public static interface AssetListener {
        void assetsReloadStarted();
        void assetsUpdateStarted();
        void assetsUpdated(boolean updated);
        void assetsReloadError(Exception e);
    }

    private static class AssetsListenerList extends WTKListenerList<AssetListener> implements AssetListener {
        @Override
        public void assetsReloadStarted() {
            for (AssetListener listener : this) {
                listener.assetsReloadStarted();
            }
        }

        @Override
        public void assetsUpdateStarted() {
            for (AssetListener listener : this) {
                listener.assetsUpdateStarted();
            }
        }

        @Override
        public void assetsUpdated(boolean updated) {
            for (AssetListener listener : this) {
                listener.assetsUpdated(updated);
            }
        }

        @Override
        public void assetsReloadError(Exception e) {
            for(AssetListener listener : this) {
                listener.assetsReloadError(e);
            }
        }
    }

    private static class ProgressListeners extends WTKListenerList<ProgressListener> implements ProgressListener {

        @Override
        public void progressStarted(long total) {
            for (ProgressListener listener : this) {
                listener.progressStarted(total);
            }
        }

        @Override
        public void progressUpdate(long soFar) {
            for (ProgressListener listener : this) {
                listener.progressUpdate(soFar);
            }
        }

        @Override
        public void progressFinished() {
            for (ProgressListener listener : this) {
                listener.progressFinished();
            }
        }

        @Override
        public void progressAborted(Exception e) {
            for (ProgressListener listener : this) {
                listener.progressAborted(e);
            }
        }
    }

    private AssetsListenerList assetsListeners = new AssetsListenerList();
    private ProgressListeners progressListeners = new ProgressListeners();

    private ApplicationData applicationData;

    public AssetManager(ApplicationData applicationData) {
        this.applicationData = applicationData;
        applicationData.setUserSettings(UserSettingsHelper.loadSettings());
    }

    public ListenerList<AssetListener> getAssetsListeners() {
        return assetsListeners;
    }

    public ListenerList<ProgressListener> getProgressListeners() {
        return progressListeners;
    }

    public void initializeAssets() {
        UserSettings userSettings = applicationData.getUserSettings();
        final String key = userSettings.getApiKeyInfo().getKey();
        final String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return; // nothing to initialize
        }

        SyncUtils.executeLocked(lock, new Runnable() {
            @Override
            public void run() {
                try {
                    EveAssets assets = loadAssets();
                    if (assets != null) { // there are local assets
                        applicationData.setAssets(assets);
                        IndustryJobModel industryJobModel = IndustryJobLoader.loadBackupIndustryJobs(Constants.applicationPath, Constants.industryJobsFileName, applicationData);
                        if (industryJobModel != null) {
                            updateJobStatus(assets, industryJobModel);
                        } else {
                            log.warn("No backup found for industry jobs corresponding to the assets. Planner might provide inaccurate data.");
                        }
                    } else {
                        reloadAssets();
                    }
                } catch (SQLException | ParsingException | IOException e) {
                    log.error("Unable to initialize assets", e);
                }
            }
        });
    }

    public boolean reloadAssets() throws ParsingException, SQLException, IOException {
        UserSettings userSettings = applicationData.getUserSettings();
        final String key = userSettings.getApiKeyInfo().getKey();
        final String vCode = userSettings.getApiKeyInfo().getvCode();
        if (key.isEmpty() || vCode.isEmpty()) {
            log.warn("There is no api key or/and vCode");
            return false;
        }

        return  SyncUtils.executeLocked(lock, new SyncUtils.Callback<Boolean>() {
            @Override
            public Boolean run() {
                // check if server actually has new data
                boolean updated = false;
                if (!isDataUpToDate()) {
                    try {
                        assetsListeners.assetsReloadStarted();
                        MessageFormat format = new MessageFormat(Constants.apiCallCorpAssets);
                        String url = format.format(new Object[]{key, vCode});
                        String fileName = getLocalAssetsFileName();
                        IndustryJobLoader.backupLastIndustryUpdate(Constants.applicationPath, Constants.industryJobsFileName, applicationData);
                        HttpHelper.loadFileFromWeb(url, Constants.applicationPath, fileName, progressListeners);

                        assetsListeners.assetsUpdateStarted();
                        EveAssets assets = loadAssets();
                        if (updated = (assets != null && (applicationData.getAssets() == null || !applicationData.getAssets().getCurrentTime().equals(assets.getCurrentTime())))) {
                            applicationData.setAssets(assets);
                            updateJobStatus(assets, applicationData.getIndustryJobs());
                        }

                        assetsListeners.assetsUpdated(updated);
                    } catch (Exception e) {
                        assetsListeners.assetsReloadError(e);
                    }
                }

                return updated;
            }
        });
    }

    private void updateJobStatus(EveAssets assets, IndustryJobModel industryJobs) {
        // update set of non-delivered jobs for correct planning
        Date assetsUpdated = assets.getCurrentTime();
        Set<Long> notDeliveredJobIds = new HashSet<>();
        for (EveIndustryJob eveIndustryJob : industryJobs.getAllJobs()) {
            if (eveIndustryJob.getEndProductionTime().before(assetsUpdated) && !eveIndustryJob.isDelivered()) {
                notDeliveredJobIds.add(eveIndustryJob.getId());
            }
        }

        applicationData.getIndustryJobs().setJobIdsNotDeliveredBeforeAssetsUpdate(notDeliveredJobIds);
    }

    public void reloadLocalAssets() {
        SyncUtils.executeLocked(lock, new Runnable() {
            @Override
            public void run() {
                try {
                    assetsListeners.assetsUpdateStarted();
                    EveAssets assets = loadAssets();
                    if (assets != null) {
                        applicationData.setAssets(assets);
                    }

                    assetsListeners.assetsUpdated(assets != null);
                } catch (SQLException | ParsingException | IOException e) {
                    assetsListeners.assetsReloadError(e);
                    log.error("Unable to load assets", e);
                }
            }
        });
    }

    private String getLocalAssetsFileName() {
        String key = applicationData.getUserSettings().getApiKeyInfo().getKey();
        return Constants.localAssetFileName + "." + key;
    }

    private boolean isDataUpToDate() {
        return applicationData.getAssets() != null && applicationData.getAssets().getCachedUntil().getTime() > System.currentTimeMillis();
    }

    private EveAssets loadAssets() throws ParsingException, SQLException, IOException {
        EveAssets assets = null;

        Path localAssetsFilePath = Constants.applicationPath.resolve(getLocalAssetsFileName());
        if (localAssetsFilePath != null && Files.exists(localAssetsFilePath)) {
            try (Reader reader = new FileReader(localAssetsFilePath.toFile())) {
                assets = EveItemLoader.getInstance().loadAssetsFromXml(reader);
            }
        } else {
            log.warn("File does not exist: " + localAssetsFilePath);
        }

        return assets;
    }


}
