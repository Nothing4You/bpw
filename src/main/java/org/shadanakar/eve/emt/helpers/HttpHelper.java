/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.helpers;

import org.apache.commons.compress.utils.CountingOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HttpHelper {
    private static final Log log = LogFactory.getLog(HttpHelper.class);
    public static final String dateFormatPattern = "EEE, dd MMM yyyy HH:mm:ss zzz";


    private static interface CountingProgressListener {
        void update(long n);
    }

    private static class DownloadCountingOutputStream extends CountingOutputStream {
        private CountingProgressListener listener = null;

        public DownloadCountingOutputStream(OutputStream out) {
            super(out);
        }

        public void setListener(CountingProgressListener listener) {
            this.listener = listener;
        }

        @Override
        protected void count(long n) {
            super.count(n);
            if (listener != null) {
                listener.update(n);
            }
        }

    }

    public static boolean isFileUpdated(String urlStr, Path path, String fileName) {
        long localTimestamp = 0;
        long remoteTimestamp = 0;

        String timestampFileName = "." + fileName + ".timestamp";
        Path timestampFilePath = path.resolve(timestampFileName);
        try {
            List<String> lines = Files.readAllLines(timestampFilePath, Charset.forName("UTF-8"));
            if (lines.size() > 0) {
                localTimestamp = Long.parseLong(lines.get(0));
            }
        } catch (IOException e) {
            // ignore
        }

        // load remote
        try {
            URL url = new URL(urlStr);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("HEAD");
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatPattern, Locale.ENGLISH);
            Date lastModified = dateFormat.parse(urlConnection.getHeaderField("Last-Modified"));
            remoteTimestamp = lastModified.getTime();
        } catch (ParseException | IOException e) {
            // ignore
        }

        return remoteTimestamp == 0 || remoteTimestamp > localTimestamp;
    }

    public static void loadFileFromWeb(String urlStr, Path path, String fileName, final ProgressListener progressListener) throws IOException {
        long remoteTimestamp = 0;

        String timestampFileName = "." + fileName + ".timestamp";
        Path timestampFilePath = path.resolve(timestampFileName);

        URL url = new URL(urlStr);
        Path tmpPath = path.resolve(fileName + ".tmp");
        Path destinationPath = path.resolve(fileName);
        try (OutputStream os = new FileOutputStream(tmpPath.toFile());
             InputStream is = url.openStream()) {

            log.debug("Fetching: "+url.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String contentLengthHeader = urlConnection.getHeaderField("Content-Length");
            log.info("Response code: " + urlConnection.getResponseCode());
            final long total = contentLengthHeader != null ? Long.parseLong(contentLengthHeader) : -1;
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatPattern, Locale.ENGLISH);
            String lastModifiedHeader = null;
            try {
                lastModifiedHeader = urlConnection.getHeaderField("Last-Modified");
                if (lastModifiedHeader != null) {
                    Date lastModified = dateFormat.parse(lastModifiedHeader);
                    remoteTimestamp = lastModified.getTime();
                }
            } catch (ParseException ex) {
                log.warn("Unable to parse Last-Modified: " + lastModifiedHeader, ex);
            }

            DownloadCountingOutputStream countingOutputStream = new DownloadCountingOutputStream(os);
            try {
                if (progressListener != null) {
                    progressListener.progressStarted(total);

                    countingOutputStream.setListener(new CountingProgressListener() {
                        private long soFar = 0;

                        @Override
                        public void update(long n) {
                            soFar += n;
                            progressListener.progressUpdate(soFar);
                        }
                    });

                }


                IOUtils.copy(is, countingOutputStream);
                if (progressListener != null) {
                    progressListener.progressFinished();
                }
            } catch (IOException e) {
                if (progressListener != null) {
                    progressListener.progressAborted(e);
                }
            }
        } catch (IOException e) {
            Files.delete(tmpPath);
            throw e;
        }

        Files.move(tmpPath, destinationPath, StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING);

        try {
            if (remoteTimestamp != 0) {
                Files.write(timestampFilePath, String.valueOf(remoteTimestamp).getBytes(Charset.forName("UTF-8")));
            }
        } catch (IOException e) {
            log.error("IO error: " + timestampFilePath.toString(), e);
        }
    }
}
