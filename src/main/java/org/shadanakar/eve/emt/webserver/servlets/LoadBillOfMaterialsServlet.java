/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.webserver.servlets;

import org.shadanakar.eve.emt.model.JobMaterialModel;
import org.shadanakar.eve.emt.webserver.WebServerData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoadBillOfMaterialsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        WebServerData data = WebServerData.getInstance();
        out.print("{");
        out.printf(
                "totalVolume: \"%,#.2f\", " +
                "totalPrice: \"%,#.2f\", ",
                data.getTotalVolumeMissing(), data.getTotalPriceMissing());

        out.print("items: [ ");

        boolean comma = false;

        for (JobMaterialModel jobMaterialModel : data.getBillOfMaterials()) {
            if (jobMaterialModel.getQuantityMissing() != 0) {
                if (comma) {
                    out.print(", ");
                } else {
                    comma = true;
                }
                out.printf("{eveId: %d, name: \"%s\", quantity: \"%,d\", price: \"%,#.2f\"}",
                        jobMaterialModel.getEveType().getId(), jobMaterialModel.getEveType().getName(),
                        (int)jobMaterialModel.getQuantityMissing(), jobMaterialModel.getPrice());
            }
//            "{id: 1, eveId: 2006, name: \"Omen\", quantity: \"2,100\"}," +
//            "{id: 2, eveId: 438, name: \"1MN Afterburner II\", quantity: \"100\"}," +

        }
        out.print(" ]");
        out.print("}");
    }
}
