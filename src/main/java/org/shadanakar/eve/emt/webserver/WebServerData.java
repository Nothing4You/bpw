/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.webserver;

import org.shadanakar.eve.emt.model.JobMaterialModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WebServerData {
    private List<JobMaterialModel> billOfMaterials;
    private float totalVolumeMissing;
    private float totalPriceMissing;

    private WebServerData(){}

    public static WebServerData getInstance() {
        return InstanceHolder._instance;
    }

    public void setBillOfMaterials(List<JobMaterialModel> billOfMaterials) {
        this.billOfMaterials = new ArrayList<>(billOfMaterials); // copy

        // sort by name
        Collections.sort(this.billOfMaterials, new Comparator<JobMaterialModel>() {
            @Override
            public int compare(JobMaterialModel o1, JobMaterialModel o2) {
                return o1.getEveType().getName().toLowerCase().compareTo(o2.getEveType().getName().toLowerCase());
            }
        });
    }

    public List<JobMaterialModel> getBillOfMaterials() {
        return billOfMaterials;
    }

    public void setTotalVolumeMissing(float totalVolumeMissing) {
        this.totalVolumeMissing = totalVolumeMissing;
    }

    public float getTotalVolumeMissing() {
        return totalVolumeMissing;
    }

    public void setTotalPriceMissing(float totalPriceMissing) {
        this.totalPriceMissing = totalPriceMissing;
    }

    public float getTotalPriceMissing() {
        return totalPriceMissing;
    }


    private static class InstanceHolder {
        private static final WebServerData _instance = new WebServerData();
    }
}
