/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.model.LocalItemInfo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ApplicationHelper {
    private static final Log log = LogFactory.getLog(ApplicationHelper.class);

    private static final ExecutorService executor = Executors.newCachedThreadPool();

    public static ExecutorService getExecutor() {
        return executor;
    }


    private ApplicationHelper() {}

    public static void scheduleSave(final LocalItemInfo localItemInfo, final Callback<Integer> actionOnSuccess) {
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    LocalDbHelper.getInstance().saveLocalItemInfo(localItemInfo);
                    if (actionOnSuccess != null) {
                        actionOnSuccess.handle(0);
                    }
                } catch (Exception e) {
                    log.error(e);
                }
            }
        });
    }
}
