/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.beans.BXMLSerializer;
import org.apache.pivot.serialization.SerializationException;
import org.apache.pivot.wtk.*;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.model.EveType;
import org.shadanakar.eve.emt.model.LocalBlueprintInfo;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.ui.model.ImportFromClipboardTableLine;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ImportFromClipboardDialogHelper {
    private final static Log log = LogFactory.getLog(ImportFromClipboardDialogHelper.class);

    private final Dialog dialog;
    private final Window window;
    private final ApplicationData applicationData; // apparently this is not used....
    private final TablePane.Row pageContentPane;
    private final PushButton nextButton;
    private final PushButton prevButton;
    private final Page1Helper pageOneHelper;
    private final Page2Helper pageTwoHelper;
    private int currentPage;

    public ImportFromClipboardDialogHelper(Window window, ApplicationData applicationData) throws IOException, SerializationException {
        this.window = window;
        this.applicationData = applicationData;
        BXMLSerializer serializer = new BXMLSerializer();
        dialog = (Dialog) serializer.readObject(ImportFromClipboardDialogHelper.class, "dialog-import-from-clipboard.bxml");
        pageContentPane = (TablePane.Row) serializer.getNamespace().get("dialogPageContent");
        nextButton = (PushButton) serializer.getNamespace().get("nextButton");
        prevButton = (PushButton) serializer.getNamespace().get("prevButton");

        // load pages
        pageOneHelper = new Page1Helper();
        pageTwoHelper = new Page2Helper();

        pageContentPane.add(pageOneHelper.getPanel());

        nextButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                if (currentPage == 1) {
                    processInput(pageOneHelper.getText());
                } else {
                    finish();
                }
            }
        });

        currentPage = 1;

        prevButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                backToPageOne();
            }
        });
    }

    private void backToPageOne() {
        currentPage = 1;
        pageContentPane.remove(pageTwoHelper.getPanel());
        pageContentPane.add(pageOneHelper.getPanel());
        prevButton.setEnabled(false);
        nextButton.setButtonData("Next");
    }

    private void processInput(final String text) {
        disableEverything();
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() { // todo: this method may need some refactoring
                String lines[] = text.split("\n");
                for (int i = 0; i < lines.length; i++) {
                    lines[i] = lines[i].trim();
                }

                log.info("Found " + lines.length + " lines.. removing duplicates");

                List<String> lineList = new ArrayList<>(new HashSet<>(Arrays.asList(lines)));
                Collections.sort(lineList); // by name
                log.info("After unique: " + lineList.size() + " lines left... processing");

                Map<Integer, ImportFromClipboardTableLine> foundBlueprintTypes = new LinkedHashMap<>();

                for (String line : lineList) {
                    String columns[] = line.split("\t");
                    if (columns.length < 7) {
                        log.warn("Unable to process line, expecting at least 7 tokens got " + columns.length + ": " + line);
                        continue;
                    }

                    try {
                        final String typeName = columns[0];
//                        final String quantity = columns[1];
//                        final String groupName = columns[2];
//                        final String categoryName = columns[3];
                        final String s_isCopy = columns[4];
                        final String ml = columns[5];
                        final String pl = columns[6];
                        final String runs = columns.length > 7 ? columns[7] : "";

                        EveType type = EveDbHelper.getInstance().findTypeByName(typeName);

                        if (type == null) {
                            log.warn(typeName + " is not found in item database, skipping line: " + line);
                            continue;
                        }

                        // type check
                        if (!type.isBlueprint()) {
                            log.warn("Item is not a blueprint - skipping: " + line);
                            continue;
                        }

                        LocalBlueprintInfo oldBlueprintInfo = LocalDbHelper.getInstance().loadLocalBlueprintInfo(type.getId());

                        LocalBlueprintInfo newBlueprintInfo = new LocalBlueprintInfo(type.getId());
                        newBlueprintInfo.setRuns(runs.isEmpty() ? null : new Integer(runs));
                        newBlueprintInfo.setMaterialLevel(ml.isEmpty() ? null : new Integer(ml));
                        newBlueprintInfo.setProductivityLevel(pl.isEmpty() ? null : new Integer(pl));

                        boolean isCopy = s_isCopy.equalsIgnoreCase("yes");

                        int defaultME = !isCopy || type.getBlueprint().getTechLevel() != 2 ? 0 : -4;
                        int defaultPE = !isCopy || type.getBlueprint().getTechLevel() != 2 ? 0 : -4;
                        int defaultRuns = !isCopy || type.getBlueprint().getTechLevel() != 2 ? type.getBlueprint().getMaxProductionLimit() : type.getBlueprint().getMaxProductionLimit() / 10;
                        if (oldBlueprintInfo.equalsFuzzy(newBlueprintInfo, defaultME, defaultPE, defaultRuns)) {
                            log.info(typeName + " old and new blueprint stats are equal, skipping...");
                            continue;
                        }

                        if (foundBlueprintTypes.containsKey(type.getId())) {
                            log.warn("Not good: found multiple instances of same blueprint info. Will skip this one: " + line);
                            continue;
                        }

                        foundBlueprintTypes.put(type.getId(), new ImportFromClipboardTableLine(type, oldBlueprintInfo, newBlueprintInfo));
                    } catch (RuntimeException | SQLException e) {
                        log.error("Error while processing line (skipped): " + line, e);
                    }
                }

                Collection<ImportFromClipboardTableLine> values = foundBlueprintTypes.values();

                final org.apache.pivot.collections.List<ImportFromClipboardTableLine> tableContent =
                        new org.apache.pivot.collections.ArrayList<>(values.toArray(new ImportFromClipboardTableLine[values.size()]));

                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        pageTwoHelper.setTableContent(tableContent);
                        toPageTwo();
                    }
                });
            }
        });
    }

    private void toPageTwo() {
        currentPage = 2;
        pageContentPane.remove(pageOneHelper.getPanel());
        pageContentPane.add(pageTwoHelper.getPanel());
        nextButton.setButtonData("Finish");
        nextButton.setEnabled(true);
        prevButton.setEnabled(true);
        pageTwoHelper.enable(); // for future use...
        pageOneHelper.enable(); // for future use...
    }

    private void finish() {
        disableEverything();
        final org.apache.pivot.collections.List<ImportFromClipboardTableLine> tableData = pageTwoHelper.getTableData();
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                // save checked items
                for (ImportFromClipboardTableLine importFromClipboardTableLine : tableData) {
                    if (importFromClipboardTableLine.getApplyCheckbox()) {
                        LocalBlueprintInfo localBlueprintInfoToSave = importFromClipboardTableLine.getNewBlueprintInfo();
                        try {
                            LocalDbHelper.getInstance().saveLocalBlueprintInfo(localBlueprintInfoToSave);
                        } catch (SQLException e) {
                            log.error("Unable to save blueprint info " + localBlueprintInfoToSave.dump(), e);
                        }
                    }
                }

                // close dialog
                ApplicationContext.queueCallback(new Runnable() {
                    @Override
                    public void run() {
                        dialog.close(true);
                    }
                });
            }
        });
    }

    private void disableEverything() {
        pageOneHelper.disable();
        pageTwoHelper.disable();
        nextButton.setEnabled(false);
        prevButton.setEnabled(false);
    }

    public void start(final Runnable executeOnSuccess) {
        dialog.open(window, new DialogStateListener.Adapter() {
            @Override
            public void dialogClosed(Dialog dialog, boolean modal) {
                if (dialog.getResult()) {
                    executeOnSuccess.run();
                }
            }
        });
    }

    private static class Page1Helper {
        final Component pane;
        final TextArea clipboardContent;

        private Page1Helper() throws IOException, SerializationException {
            BXMLSerializer serializer = new BXMLSerializer();
            pane = (Component) serializer.readObject(Page1Helper.class, "dialog-import-from-clipboard-page-1.bxml");
            clipboardContent = (TextArea) serializer.getNamespace().get("clipboardContent");
        }

        public String getText() {
            return clipboardContent.getText();
        }

        public Component getPanel() {
            return pane;
        }

        public void disable() {
            clipboardContent.setEnabled(false);
        }

        public void enable() {
            clipboardContent.setEnabled(true);
        }
    }

    private static class Page2Helper {
        final Component pane;
        final TableView preImportTableView;

        private Page2Helper() throws IOException, SerializationException {
            BXMLSerializer serializer = new BXMLSerializer();
            pane = (Component) serializer.readObject(Page1Helper.class, "dialog-import-from-clipboard-page-2.bxml");
            preImportTableView = (TableView) serializer.getNamespace().get("preImportTableView");

            preImportTableView.getComponentMouseButtonListeners().add(new ComponentMouseButtonListener.Adapter() {
                @Override
                @SuppressWarnings("unchecked")
                public boolean mouseClick(Component comp, Mouse.Button button, int x, int y, int count) {
                    if (button == Mouse.Button.LEFT) {
                        org.apache.pivot.collections.List<ImportFromClipboardTableLine> customTableData =
                                (org.apache.pivot.collections.List<ImportFromClipboardTableLine>) preImportTableView.getTableData();

                        int columnIndex = preImportTableView.getColumnAt(x);
                        if (columnIndex == 0) {
                            int rowIndex = preImportTableView.getRowAt(y);
                            ImportFromClipboardTableLine row = customTableData.get(rowIndex);

                            row.setApplyCheckbox(!row.getApplyCheckbox());
                            customTableData.update(rowIndex, row);
                        }
                    }

                    return false;
                }
            });
        }

        public void setTableContent(org.apache.pivot.collections.List<ImportFromClipboardTableLine> tableContent) {
            preImportTableView.setTableData(tableContent);
        }

        public Component getPanel() {
            return pane;
        }

        public void enable() {
            preImportTableView.setEnabled(true);
        }

        public void disable() {
            preImportTableView.setEnabled(false);
        }

        @SuppressWarnings("unchecked")
        public org.apache.pivot.collections.List<ImportFromClipboardTableLine> getTableData() {
            return (org.apache.pivot.collections.List<ImportFromClipboardTableLine>) preImportTableView.getTableData();
        }
    }
}
