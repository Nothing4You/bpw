/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.controller;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shadanakar.eve.emt.db.EveDbHelper;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;
import org.shadanakar.eve.emt.helpers.ImageLoader;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.ui.model.BlueprintTableLine;

import javax.annotation.Nullable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManufacturingController {
    private static final Log log = LogFactory.getLog(ManufacturingController.class);

    /**
     * @return first - manufacturing price, second - manufacturing time (sec). Both per item;
     * @throws SQLException
     */
    public static TimeAndPriceData compileBillOfMaterialsTableData(LocalBlueprintInfo localBlueprintInfo, EveType t1blueprintType, float inventionProbability, EveType blueprintType, ApplicationData applicationData, org.apache.pivot.collections.List<BlueprintTableLine> resultList) throws SQLException {
        float manufacturingPrice = 0f;
        float manufacturingTimeSec = BlueprintHelper.calculateProductionTime(
                BlueprintHelper.calculateProductivityLevel(localBlueprintInfo, blueprintType.getBlueprint(), BlueprintType.Copy).getValue(),
                blueprintType.getBlueprint());
        float inventionAndCopyTimeSec = t1blueprintType != null ?
                BlueprintHelper.calculateInventionTime(t1blueprintType) + BlueprintHelper.calculateCopyTime(t1blueprintType) : 0f;

        EveType productType = blueprintType.getBlueprint().getProductType();
        List<EveMaterialType> materialTypes = EveDbHelper.getInstance().getMaterialTypes(productType.getId());

        // we are going to copy this in order to be able to remove stuff from this list.
        List<EveMaterialType> extraMaterials = new ArrayList<>(EveDbHelper.getInstance().getExtraMaterialTypes(blueprintType.getId()));

        // Compiling final list
        // 1. processing materialTypes... applying waste in progress

        float wasteCoefficient = BlueprintHelper.calculateWasteFactor(blueprintType.getBlueprint(), 
                BlueprintHelper.calculateMaterialLevel(localBlueprintInfo, blueprintType.getBlueprint(), BlueprintType.Copy).getValue());

        // calculate recycled stuff if applicable
        Map<Integer, Integer> recycledMaterials = new HashMap<>();
        for (EveMaterialType type : extraMaterials) {
            if(type.isRecycle()) {
                List<EveMaterialType> recycleMaterials = EveDbHelper.getInstance().getMaterialTypes(type.getMaterialType().getId());
                for (EveMaterialType recycleMaterial : recycleMaterials) {
                    int materialId = recycleMaterial.getMaterialType().getId();
                    int previousQuantity = recycledMaterials.containsKey(materialId) ? recycledMaterials.get(materialId) : 0;
                    recycledMaterials.put(materialId, previousQuantity + (int) recycleMaterial.getQuantity());
                }
            }
        }

        for (final EveMaterialType type: materialTypes) {
            BlueprintTableLine line = new BlueprintTableLine();

            line.setName(type.getMaterialType().getName());
            line.setIcon(ImageLoader.getInstance().loadImage16x16(type.getMaterialType().getId()));

            int quantity = (int) type.getQuantity();

            // apply recycled stuff.
            if (recycledMaterials.containsKey(type.getMaterialType().getId())) {
                quantity -= recycledMaterials.get(type.getMaterialType().getId());
            }

            int wasteQuantity = Math.round(quantity*wasteCoefficient);

            EveMaterialType extraType = Iterables.find(extraMaterials, new Predicate<EveMaterialType>() {
                @Override
                public boolean apply(@Nullable EveMaterialType that) {
                    return that != null && that.getMaterialType() == type.getMaterialType();
                }
            }, null);

            line.setBaseQuantity(quantity);
            line.setWasteQuantity(wasteQuantity);
            if(extraType != null) {
                line.setExtraQuantity(extraType.getQuantity() * extraType.getDamagePerJob());
                extraMaterials.remove(extraType);
            }

            // only if it resulted quantity is positive
            if (line.getQuantity() > 0f) {
                line.setMarketPrice(applicationData.getMarketData().estimatePrice(MarketOrderType.Sell, type.getMaterialType(), line.getQuantity()));
                line.setManufacturingPrice(applicationData.calculateItemManufacturingPrice(type.getMaterialType()));

                if (line.getManufacturingPrice() != null && line.getManufacturingPrice() < line.getMarketPrice()) {
                    manufacturingTimeSec += line.getQuantity()*applicationData.calculateItemManufacturingTimeSec(type.getMaterialType());
                }

                manufacturingPrice += line.getQuantity()*line.getMinimalPrice();
                resultList.add(line);
            }
        }

        // Just add what is left in extra collection
        for (EveMaterialType type : extraMaterials) {
            if (type.getDamagePerJob() != 0) {
                BlueprintTableLine line = new BlueprintTableLine();

                line.setName(type.getMaterialType().getName());
                line.setIcon(ImageLoader.getInstance().loadImage16x16(type.getMaterialType().getId()));
                line.setExtraQuantity(type.getQuantity() * type.getDamagePerJob());
                line.setMarketPrice(applicationData.getMarketData().estimatePrice(MarketOrderType.Sell, type.getMaterialType(), line.getQuantity()));
                line.setManufacturingPrice(applicationData.calculateItemManufacturingPrice(type.getMaterialType()));
                manufacturingPrice += line.getQuantity()*line.getMinimalPrice();

                resultList.add(line);
            }
        }

        manufacturingPrice /= productType.getPortionSize();
        manufacturingTimeSec /= productType.getPortionSize();
        inventionAndCopyTimeSec /= (inventionProbability*productType.getPortionSize()*BlueprintHelper.calculateRuns(localBlueprintInfo, blueprintType.getBlueprint()));
        log.debug("BP: " + blueprintType.getName() + " - manufacturingTimeSec=" + manufacturingTimeSec + ", inventionAndCopyTimeSec="+inventionAndCopyTimeSec);
        return new TimeAndPriceData(manufacturingPrice, manufacturingTimeSec, inventionAndCopyTimeSec);
    }

    public static class TimeAndPriceData {
        private final float manufacturingPrice;
        private final float manufacturingTimeSec;
        private final float inventionAndCopyTimeSec;

        public TimeAndPriceData(float manufacturingPrice, float manufacturingTimeSec, float inventionAndCopyTimeSec) {

            this.manufacturingPrice = manufacturingPrice;
            this.manufacturingTimeSec = manufacturingTimeSec;
            this.inventionAndCopyTimeSec = inventionAndCopyTimeSec;
        }

        public float getManufacturingPrice() {
            return manufacturingPrice;
        }

        public float getManufacturingTimeSec() {
            return manufacturingTimeSec;
        }

        public float getInventionAndCopyTimeSec() {
            return inventionAndCopyTimeSec;
        }
    }
}
