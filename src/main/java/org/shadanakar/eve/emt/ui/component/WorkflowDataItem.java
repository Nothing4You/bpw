/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.component;

import org.apache.pivot.collections.ArrayList;
import org.apache.pivot.collections.List;

public class WorkflowDataItem {
    private float startPosition;
    private float length;
    private String label;
    private float percentComplete;
    private List<Integer> dependencies;

    /**
     *
     * @param startPosition start position at the chart
     * @param length length in some units...
     * @param label The name
     * @param percentComplete this should be between 0 and 1
     * @param dependencies list of positions of dependent dataItems
     */
    public WorkflowDataItem(float startPosition, float length, String label, float percentComplete, Integer... dependencies) {
        this.startPosition = startPosition;
        this.length = length;
        this.label = label;
        this.percentComplete = percentComplete;
        this.dependencies = new ArrayList<>(dependencies);
    }

    public float getStartPosition() {
        return startPosition;
    }

    public float getLength() {
        return length;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getPercentComplete() {
        return percentComplete;
    }

    public List<Integer> getDependencies() {
        return dependencies;
    }
}
