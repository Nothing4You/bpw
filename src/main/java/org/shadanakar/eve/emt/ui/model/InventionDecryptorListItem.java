/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.model;

import org.apache.pivot.wtk.content.ListItem;
import org.apache.pivot.wtk.media.Image;
import org.shadanakar.eve.emt.model.EveDecryptorInfo;

public class InventionDecryptorListItem extends ListItem {
    private final EveDecryptorInfo decryptor;

    public InventionDecryptorListItem(Image icon, String text, EveDecryptorInfo decryptor) {
        super(icon, text);
        this.decryptor = decryptor;
    }

    public EveDecryptorInfo getDecryptor() {
        return decryptor;
    }
}
