/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.model;

import org.apache.pivot.wtk.media.Image;

@SuppressWarnings("UnusedDeclaration")
public class JobMaterialsTableLine {
    private Image icon;
    private String name;
    private float itemPrice;
    private float price;
    private float quantityRequired;
    private int quantityToManufacture;
    private int quantityInProduction;
    private int quantityPresent;
    private float quantityMissing;
    private float volumeMissing;

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public float getQuantityRequired() {
        return quantityRequired;
    }

    public void setQuantityRequired(float quantityRequired) {
        this.quantityRequired = quantityRequired;
    }

    public int getQuantityPresent() {
        return quantityPresent;
    }

    public void setQuantityPresent(int quantityPresent) {
        this.quantityPresent = quantityPresent;
    }

    public float getQuantityMissing() {
        return quantityMissing;
    }

    public void setQuantityMissing(float quantityMissing) {
        this.quantityMissing = quantityMissing;
    }

    public float getVolumeMissing() {
        return volumeMissing;
    }

    public void setVolumeMissing(float volumeMissing) {
        this.volumeMissing = volumeMissing;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getQuantityToManufacture() {
        return quantityToManufacture == 0 ? null : quantityToManufacture;
    }

    public void setQuantityToManufacture(int quantityToManufacture) {
        this.quantityToManufacture = quantityToManufacture;
    }

    public float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Integer getQuantityInProduction() {
        return quantityInProduction == 0 ? null : quantityInProduction;
    }

    public float getDebugQuantity() {
        return quantityRequired - quantityInProduction - quantityToManufacture - quantityPresent;
    }

    public void setQuantityInProduction(int quantityInProduction) {
        this.quantityInProduction = quantityInProduction;
    }
}
