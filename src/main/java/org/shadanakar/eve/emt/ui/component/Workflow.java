/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.component;

import org.apache.pivot.collections.List;
import org.apache.pivot.util.ListenerList;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.WTKListenerList;

public class Workflow extends Component {
    private List<? extends WorkflowDataItem> workflowData;
    private static class WorkflowListenerList extends WTKListenerList<WorkflowListener> implements WorkflowListener {
        @Override
        public void workflowDataChanged(Workflow workflow, List<? extends WorkflowDataItem> previousWorkflowData) {
            for (WorkflowListener listener : this) {
                listener.workflowDataChanged(workflow, previousWorkflowData);
            }
        }
    }

    private WorkflowListenerList workflowListeners = new WorkflowListenerList();


    public Workflow() {
        setSkin(new WorkflowSkin());
    }

    public void setWorkflowData(List<? extends WorkflowDataItem> workflowData) {
        if (workflowData == null) {
            throw new IllegalArgumentException("workflowData should not be null");
        }

        List<? extends WorkflowDataItem> oldData = this.workflowData;

        if (workflowData != oldData) {
            this.workflowData = workflowData;
            workflowListeners.workflowDataChanged(this, oldData);
        }
    }

    public List<? extends WorkflowDataItem> getWorkflowData() {
        return workflowData;
    }


    public ListenerList<WorkflowListener> getWorkflowListeners() {
        return workflowListeners;
    }
}
