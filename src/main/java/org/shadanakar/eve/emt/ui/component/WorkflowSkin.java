/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui.component;

import org.apache.pivot.collections.List;
import org.apache.pivot.wtk.*;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.skin.ComponentSkin;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;

public class WorkflowSkin extends ComponentSkin implements WorkflowListener {
    private static final int BAR_HEIGHT = 24;
    private static final int UNIT_WIDTH = 20;
    private static final int BAR_RIGHT_GAP = 5;
    private static final int ZERO_BAR_WIDTH = 1;
    private static final int BAR_VERTICAL_PADDING = 3;
    private static final int PADDING_LEFT = 5;

    private final Font font;
    private Paint barColor;
    private Paint textColor;
    private Paint arrowColor;
    private Paint backgroundColor;
    private Paint barCompletedColor;

    public WorkflowSkin() {
        Theme theme = Theme.getTheme();
        font = theme.getFont();
        barColor = Color.blue;
        barCompletedColor = Color.green;
        textColor = Color.black;
        arrowColor = Color.black;
        backgroundColor = Color.white;
    }

    @Override
    public void layout() {
        // no children, no layout
    }

    @Override
    public void install(Component componentArgument) {
        super.install(componentArgument);
        Workflow workflow = (Workflow) componentArgument;
        workflow.getWorkflowListeners().add(this);
    }

    @Override
    public int getPreferredWidth(int height) {
        Workflow workflow = (Workflow) getComponent();
        List<? extends WorkflowDataItem> data = workflow.getWorkflowData();

        int width = 0;
        if (data != null) {
            for (WorkflowDataItem workflowDataItem : data) {
                int candidate = PADDING_LEFT + (int)(workflowDataItem.getStartPosition()*UNIT_WIDTH) + calculateItemWidth(workflowDataItem);
                if (candidate > width) {
                    width = candidate;
                }
            }
        }

        return width;
    }

    private int calculateItemWidth(WorkflowDataItem workflowDataItem) {
        // base width
        int widthRaw = (int) (UNIT_WIDTH * workflowDataItem.getLength());
        int width = (widthRaw != 0 ? widthRaw : ZERO_BAR_WIDTH) + BAR_RIGHT_GAP;
        String text = workflowDataItem.getLabel();
        if(text != null && !text.isEmpty()) {
            FontRenderContext fontRenderContext = Platform.getFontRenderContext();
            Rectangle2D stringBounds = font.getStringBounds(text, fontRenderContext);
            width += stringBounds.getWidth();
        }

        return width;
    }

    @Override
    public int getPreferredHeight(int width) {
        Workflow workflow = (Workflow) getComponent();

        List<? extends WorkflowDataItem> workflowData = workflow.getWorkflowData();
        if (workflowData != null) {
            return workflowData.getLength()*BAR_HEIGHT;
        } else {
            return 0;
        }
    }

    @Override
    public void paint(Graphics2D graphics) {
        Workflow workflow = (Workflow) getComponent();
        List<? extends WorkflowDataItem> workflowData = workflow.getWorkflowData();

        // paint background
        int componentWidth = getWidth();
        int componentHeight = getHeight();
        graphics.setPaint(backgroundColor);
        graphics.fillRect(0, 0, componentWidth, componentHeight);

        if (workflowData == null) {
            // nothing else to do..
            return;
        }

        FontRenderContext fontRenderContext = Platform.getFontRenderContext();
        int dataLength = workflowData.getLength();
        for (int i = 0; i < dataLength; i++) {
            WorkflowDataItem item = workflowData.get(i);
            String text = item.getLabel();
            int x = (int)(item.getStartPosition()*UNIT_WIDTH) + PADDING_LEFT;
            int xLeft = x;
            int y = i*BAR_HEIGHT + BAR_VERTICAL_PADDING;
            int rawWidth = (int) (item.getLength() * UNIT_WIDTH);
            int width = rawWidth == 0 ? ZERO_BAR_WIDTH : rawWidth;
            int height = BAR_HEIGHT - 2*BAR_VERTICAL_PADDING;
            graphics.setPaint(barColor);
            graphics.fillRect(x, y, width, height);

            graphics.setPaint(barCompletedColor);
            int percentCompleteWidth = Math.round(width*item.getPercentComplete());
            graphics.fillRect(x, y, percentCompleteWidth,height);

            x += width + BAR_RIGHT_GAP;
            // and now text
            graphics.setPaint(textColor);
            GlyphVector glyphVector = font.createGlyphVector(fontRenderContext, text);
            Rectangle2D textBounds = glyphVector.getLogicalBounds();
            double textHeight = textBounds.getHeight();
            LineMetrics lm = font.getLineMetrics("", fontRenderContext);
            float ascent = lm.getAscent();
            y = i*BAR_HEIGHT + (int) Math.round(ascent + (BAR_HEIGHT - textHeight)/2);
            graphics.drawGlyphVector(glyphVector, x, y);

            // arrows
            List<Integer> dependsOnList = item.getDependencies();
            if (dependsOnList != null) {
                for (int index : dependsOnList) {
                    if (index < workflowData.getLength() && index >= 0) {
                        x = calculateBeginArrowX(workflowData.get(index));
                        y = index*BAR_HEIGHT + BAR_HEIGHT/2;
                        graphics.setPaint(arrowColor);
                        int xTo = x + BAR_RIGHT_GAP / 2;
                        graphics.drawLine(x, y, xTo, y);
                        x = xTo;
                        int yTo = i*BAR_HEIGHT;
                        graphics.drawLine(x, y, x, yTo);
                        y = yTo;
                        if (x > xLeft - 4) {
                            xTo = xLeft - 4;
                            graphics.drawLine(x, y, xTo, y);
                            x = xTo;
                        }
                        yTo += BAR_HEIGHT/2;
                        graphics.drawLine(x, y, x, yTo);
                        y = yTo;
                        xTo = (int) (item.getStartPosition()*UNIT_WIDTH) + PADDING_LEFT;
                        graphics.drawLine(x, y, xTo, y);
                        x = xTo;
                        graphics.drawLine(x, y, x-2, y-2);
                        graphics.drawLine(x, y, x-2, y+2);
                    }
                }
            }
        }
    }

    private int calculateBeginArrowX(WorkflowDataItem item) {
        int rawWidth = (int) (item.getLength() * UNIT_WIDTH);
        int width = rawWidth == 0 ? ZERO_BAR_WIDTH : rawWidth;
        return (int)(item.getStartPosition() * UNIT_WIDTH) + width + PADDING_LEFT;
    }

    @Override
    public void workflowDataChanged(Workflow workflow, List<? extends WorkflowDataItem> previousWorkflowData) {
        invalidateComponent();
    }
}
