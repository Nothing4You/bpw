/*
 * Blueprint Wizard
 * Copyright (c) 2013 shadanakar.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.shadanakar.eve.emt.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pivot.serialization.SerializationException;
import org.apache.pivot.wtk.*;
import org.shadanakar.eve.emt.db.LocalDbHelper;
import org.shadanakar.eve.emt.helpers.BlueprintHelper;
import org.shadanakar.eve.emt.helpers.ImageLoader;
import org.shadanakar.eve.emt.helpers.JobManager;
import org.shadanakar.eve.emt.model.*;
import org.shadanakar.eve.emt.ui.component.Workflow;
import org.shadanakar.eve.emt.ui.component.WorkflowDataItem;
import org.shadanakar.eve.emt.ui.model.ApplicationData;
import org.shadanakar.eve.emt.ui.model.JobMaterialsTableLine;
import org.shadanakar.eve.emt.ui.model.JobTableLine;
import org.shadanakar.eve.emt.utils.PerformanceLogger;
import org.shadanakar.eve.emt.webserver.HttpServerManager;
import org.shadanakar.eve.emt.webserver.WebServerData;

import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

public class JobPaneHelper {
    private static final Log log = LogFactory.getLog(JobPaneHelper.class);
    private static final float CHART_SCALE_FACTOR = 1f / 12;

    private TableView jobTableView = null;
    private TableView materialsTableView = null;
    private Workflow jobWorkflow = null;
    private Label totalVolume = null;
    private Label totalPrice = null;
    private TextInput webServerAddress = null;
    private PushButton webServerControlButton = null;

    private ApplicationData applicationData;
    private JobManager jobManager;

    private void updateJobTable(List<JobGroup> jobs) {
        log.info("Updating jobs table....");

        final org.apache.pivot.collections.List<JobTableLine> resultedList = new org.apache.pivot.collections.ArrayList<>();

        // Just add what is left in extra collection
        for (JobGroup jobTypeGroupModel : jobs) {
            JobTableLine line = new JobTableLine();

            line.setIcon(ImageLoader.getInstance().loadImage16x16(jobTypeGroupModel.getBlueprintType().getId()));
            line.setType(jobTypeGroupModel.getActivityType().name());
            line.setName(jobTypeGroupModel.getBlueprintType().getName());
            line.setPlannedQuantity(jobTypeGroupModel.getQuantityTotal());
            line.setActualQuantity(jobTypeGroupModel.getQuantityFiltered());
            line.setQuantityInProduction(line.getPlannedQuantity() - line.getActualQuantity());
            line.setNumberOfRuns(jobTypeGroupModel.getNumberOfRuns());
            line.setJobGroup(jobTypeGroupModel);

            resultedList.add(line);
        }

        // easy part... lets update the table
        ApplicationContext.queueCallback(new Runnable() {
            @Override
            public void run() {
                jobTableView.clear();
                jobTableView.setTableData(resultedList);
            }
        });
    }

    private void updateMaterialsTable(ApplicationData.PlanCalculationResults calculationResults) {
        log.info("Updating materials table and workflow....");
        final org.apache.pivot.collections.List<JobMaterialsTableLine> resultedList = new org.apache.pivot.collections.ArrayList<>();

        // Build workflow

        // these are without dependencies;
        Map<Integer, JobWorkflowItem> derivativeWorkflowItems = new LinkedHashMap<>();
        // these may or may not have dependencies
        Map<Integer, JobWorkflowItem> primaryWorkflowItems = new LinkedHashMap<>();

        for (JobWorkflowItem jobWorkflowItem : calculationResults.getWorkflowItems()) {
            for (JobWorkflowItem workflowItem : jobWorkflowItem.getDependedWorkflowItems()) {
                int productTypeId = workflowItem.getProductType().getId();
                if(derivativeWorkflowItems.containsKey(productTypeId)) {
                    derivativeWorkflowItems.put(productTypeId, merge(derivativeWorkflowItems.get(productTypeId), workflowItem));
                } else {
                    derivativeWorkflowItems.put(productTypeId, workflowItem);
                }
            }

            int productTypeId = jobWorkflowItem.getProductType().getId();
            primaryWorkflowItems.put(productTypeId, jobWorkflowItem);
        }

        // material table and corrections to workflow
        float totalVolumeMissing = 0f;
        float totalPriceMissing = 0f;

        for (JobMaterialModel material : calculationResults.getJobMaterials()) {
            JobMaterialsTableLine line = new JobMaterialsTableLine();

            line.setIcon(ImageLoader.getInstance().loadImage16x16(material.getEveType().getId()));
            line.setName(material.getEveType().getName());
            float price = material.getPrice()*material.getQuantityMissing();
            line.setItemPrice(material.getPrice());
            line.setPrice(price);
            line.setQuantityRequired(material.getQuantityRequired());
            line.setQuantityPresent(material.getQuantityPresent());
            line.setQuantityMissing(material.getQuantityMissing());
            float volumeMissing = material.getVolumeMissing();
            totalVolumeMissing += volumeMissing;
            line.setVolumeMissing(volumeMissing);
            line.setQuantityToManufacture(material.getQuantityToManufacture());
            line.setQuantityInProduction(Math.round(material.getQuantityInProduction()));

            totalPriceMissing += price;

            resultedList.add(line);

            // workflow corrections (ram, missing, roundups and so on)
            if (derivativeWorkflowItems.containsKey(material.getEveType().getId())) {
                derivativeWorkflowItems.get(material.getEveType().getId()).setQuantity(material.getQuantityInProduction() + material.getQuantityToManufacture());
            }
        }

        String pattern = "{0, number, #,###.00}";
        final String totalVolumeLabelText =
                MessageFormat.format(pattern, totalVolumeMissing);
        final String totalPriceLabelText =
                MessageFormat.format(pattern, totalPriceMissing);

        // update web server data
        WebServerData.getInstance().setBillOfMaterials(calculationResults.getJobMaterials());
        WebServerData.getInstance().setTotalVolumeMissing(totalVolumeMissing);
        WebServerData.getInstance().setTotalPriceMissing(totalPriceMissing);


        // easy part... lets update the table
        ApplicationContext.queueCallback(new Runnable() {
            @Override
            public void run() {
                materialsTableView.clear();
                materialsTableView.setTableData(resultedList);
                totalVolume.setText(totalVolumeLabelText);
                totalPrice.setText(totalPriceLabelText);
                webServerControlButton.setEnabled(true);
            }
        });

        // workflow is ready (kinda)
        // lets build chart
        final org.apache.pivot.collections.List<WorkflowDataItem> workflowDataItems = new org.apache.pivot.collections.ArrayList<>();

//        // todo number of lines - make it configurable
//        int numberOfManufacturingLines = 20;
//        int numberOfInventionLines = 10;
//
//        // Manufacturing
//        List<Float> manufacturingLinePositions = new ArrayList<>(numberOfManufacturingLines);
//        for (int i = 0; i<numberOfManufacturingLines; ++i) {
//            manufacturingLinePositions.add(0f);
//        }
//        List<Float> inventionLinePositions = new ArrayList<>(numberOfManufacturingLines);
//        for (int i = 0; i<numberOfInventionLines; ++i) {
//            inventionLinePositions.add(0f);
//        }

        // product id -> position,end
        Map<Integer, Pair<Integer, Float>> dependencyMap = new HashMap<>();

        while(!primaryWorkflowItems.isEmpty()) {
            // select a line
            Float manufacturingStart = 0f;
            Float inventionStart = 0f;
//            Float manufacturingStart = Collections.min(manufacturingLinePositions);
//            Float inventionStart = Collections.min(inventionLinePositions);
//            int manufacturingLineNumber = manufacturingLinePositions.indexOf(manufacturingStart);
//            int inventionLineNumber = inventionLinePositions.indexOf(inventionStart);
            // install job
            if (!derivativeWorkflowItems.isEmpty()) {
                Map.Entry<Integer, JobWorkflowItem> nextEntry = derivativeWorkflowItems.entrySet().iterator().next();
                JobWorkflowItem value = nextEntry.getValue();
                Integer key = nextEntry.getKey();
                float productionTimeHours = value.getQuantity()*BlueprintHelper.calculateProductionTime(value.getProductivityLevel(), value.getBlueprintType().getBlueprint())/3600;
                float percentComplete = jobManager.estimateJobPercentComplete(value, calculationResults.getNotDeliveredJobIds(), productionTimeHours);
                String text = value.getBlueprintType().getBlueprint().getProductType().getName() + " (" + value.getQuantity() + ")";
                log.debug(value.getActivityType() + " " + value.getBlueprintType() + ": percentComplete=" + percentComplete);
                WorkflowDataItem dataItem = new WorkflowDataItem(manufacturingStart*CHART_SCALE_FACTOR, productionTimeHours*CHART_SCALE_FACTOR, text, percentComplete);
                int index = workflowDataItems.add(dataItem);
                dependencyMap.put(value.getBlueprintType().getBlueprint().getProductType().getId(), Pair.create(index, manufacturingStart + productionTimeHours));
//                manufacturingLinePositions.set(manufacturingLineNumber, manufacturingStart + productionTimeHours);
                derivativeWorkflowItems.remove(key);
            } else {
                Map.Entry<Integer, JobWorkflowItem> nextEntry = primaryWorkflowItems.entrySet().iterator().next();
                JobWorkflowItem value = nextEntry.getValue();
                Integer key = nextEntry.getKey();
                float earliestStart = 0f;
                List<Integer> dependencyIndexes = new ArrayList<>();
                for (JobWorkflowItem dependedWorkflowItem : nextEntry.getValue().getDependedWorkflowItems()) {
                    if (dependencyMap.containsKey(dependedWorkflowItem.getProductType().getId())) {
                        Pair<Integer, Float> dependency = dependencyMap.get(dependedWorkflowItem.getProductType().getId());
                        dependencyIndexes.add(dependency.getFirst());
                        earliestStart = Math.max(earliestStart, dependency.getSecond());
                    }
                }

                manufacturingStart = Math.max(earliestStart,manufacturingStart);
                float productionTimeHours;
                Float start;
                String text;
                if(value.getActivityType() == ActivityType.MANUFACTURING) {
                    productionTimeHours = value.getQuantity()*BlueprintHelper.calculateProductionTime(value.getProductivityLevel(), value.getBlueprintType().getBlueprint())/3600;
                    start = manufacturingStart;
//                    manufacturingLinePositions.set(manufacturingLineNumber, manufacturingStart + productionTimeHours);
                    text = value.getBlueprintType().getBlueprint().getProductType().getName() + " (" + value.getQuantity() + ")";
                } else {
                    productionTimeHours = value.getQuantity()*BlueprintHelper.calculateInventionTime(value.getBlueprintType())/3600;
                    start = inventionStart;
//                    inventionLinePositions.set(inventionLineNumber, inventionStart + productionTimeHours);
                    text = value.getBlueprintType().getName() + " (" + value.getQuantity() + ")";
                }
                float percentComplete = jobManager.estimateJobPercentComplete(value, calculationResults.getNotDeliveredJobIds(), productionTimeHours);
                log.debug(value.getActivityType() + " " + value.getBlueprintType() + ": percentComplete=" + percentComplete);
                WorkflowDataItem dataItem = new WorkflowDataItem(start*CHART_SCALE_FACTOR, productionTimeHours*CHART_SCALE_FACTOR, text, percentComplete, dependencyIndexes.toArray(new Integer[dependencyIndexes.size()]));
                workflowDataItems.add(dataItem);
                primaryWorkflowItems.remove(key);
            }
        }

        ApplicationContext.queueCallback(new Runnable() {
            @Override
            public void run() {
                jobWorkflow.setWorkflowData(workflowDataItems);
            }
        });
    }

    private JobWorkflowItem merge(JobWorkflowItem jobWorkflowItem, JobWorkflowItem workflowItem) {
        List<EveMaterialType> materialList = Collections.emptyList();
        return new JobWorkflowItem(ActivityType.MANUFACTURING, jobWorkflowItem.getBlueprintType(), jobWorkflowItem.getProductivityLevel(),
                jobWorkflowItem.getQuantity() + workflowItem.getQuantity(), materialList);
    }

    public void init(final BpwApplication parent, org.apache.pivot.collections.Map<String, Object> jobPaneNamespace, final ApplicationData applicationData, final JobManager jobManager) throws IOException, SerializationException {
        this.applicationData = applicationData;
        this.jobManager = jobManager;
        jobTableView = (TableView) jobPaneNamespace.get("jobsTableView");
        materialsTableView = (TableView) jobPaneNamespace.get("materialsTableView");
        totalVolume = (Label) jobPaneNamespace.get("totalVolume");
        totalPrice = (Label) jobPaneNamespace.get("totalPrice");
        jobWorkflow = (Workflow) jobPaneNamespace.get("jobWorkflow");
        webServerAddress = (TextInput) jobPaneNamespace.get("webServerAddress");
        webServerControlButton = (PushButton) jobPaneNamespace.get("webServerControlButton");

        webServerAddress.setText("not running");

        webServerControlButton.setEnabled(false);
        webServerControlButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                final HttpServerManager httpServerManager = HttpServerManager.getInstance();
                webServerControlButton.setEnabled(false);
                ApplicationHelper.getExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (httpServerManager.isRunning()) {
                            httpServerManager.stop();
                        } else {
                            httpServerManager.start();
                        }

                        ApplicationContext.queueCallback(new Runnable() {
                            @Override
                            public void run() {
                                webServerControlButton.setEnabled(true);
                                if (httpServerManager.isRunning()) {
                                    webServerControlButton.setButtonData("Stop Web Server");
                                    webServerAddress.setText(httpServerManager.getUrl());
                                } else {
                                    webServerControlButton.setButtonData("Start Web Server");
                                    webServerAddress.setText("not running");
                                }
                            }
                        });
                    }
                });

            }
        });

        jobTableView.getComponentKeyListeners().add(new ComponentKeyListener.Adapter(){
            @Override
            public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
                if (keyCode == 127) {// del
                    final JobTableLine line = (JobTableLine) jobTableView.getSelectedRow();
                    if(line != null) {
                        ApplicationHelper.getExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    LocalDbHelper.getInstance().removeJobs(line.getJobGroup().getAllJobs());
                                } catch (SQLException ex) {
                                    log.error("Unable to remove jobs from batch {" + line.getName() + ", " + line.getType() + "}", ex);
                                }
                            }
                        });
                        applicationData.getPlan().removeJobs(line.getJobGroup().getAllJobs());
                        update();
                        parent.updateTree();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void update() {
        ApplicationHelper.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try (PerformanceLogger ignored = new PerformanceLogger(log, "JobPaneHelper::update")) {
                    ApplicationData.PlanCalculationResults calculationResults = applicationData.calculatePlanBillOfMaterials(applicationData.getDecisionMaker());
                    updateJobTable(calculationResults.getJobGroups());
                    updateMaterialsTable(calculationResults);
                } catch (SQLException ex) {
                    log.error("Unable to update job table: " + ex.getMessage(), ex);
                }
            }
        });
    }
}
